import { Component, OnInit } from '@angular/core';
import SwiperCore, { A11y, Navigation, Pagination, Scrollbar } from 'swiper/core';

@Component({
  selector: 'app-ditricts',
  templateUrl: './ditricts.component.html',
  styleUrls: ['./ditricts.component.css']
})
export class DitrictsComponent implements OnInit {

  constructor() { 
    SwiperCore.use([Navigation, Pagination, Scrollbar, A11y]);
  }

  ngOnInit(): void {
  }


  onSwiper(swiper) {
    console.log(swiper);
  }
  onSlideChange() {
    console.log('slide change');
  }

}
