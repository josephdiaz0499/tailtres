import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DitrictsComponent } from './ditricts.component';

describe('DitrictsComponent', () => {
  let component: DitrictsComponent;
  let fixture: ComponentFixture<DitrictsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DitrictsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DitrictsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
