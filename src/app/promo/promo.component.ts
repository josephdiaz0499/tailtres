import { Component, OnInit } from '@angular/core';
import SwiperCore, { Autoplay, Pagination, SwiperOptions } from 'swiper';

@Component({
  selector: 'app-promo',
  templateUrl: './promo.component.html',
  styleUrls: ['./promo.component.css']
})
export class PromoComponent implements OnInit {
  config2: SwiperOptions = {
    pagination: {
                  clickable: true,
                
                },       
    // centeredSlides: true,
    // spaceBetween: 40,
   /*  watchSlidesVisibility:true, */
  //  scrollbar: true,
    loop: true,
    slidesPerView: 1,
    slideActiveClass : "swiper-promo-active",
    breakpoints:{

      320:{
        // slidesPerView: 1,
        spaceBetween: 20,
      },
      475:{
        spaceBetween: 0
      },
      768:{
        // slidesPerView: 1,
        /* centeredSlides: true, */
        spaceBetween: 0,
      },
      1024:{
        // slidesPerView: 1,
        spaceBetween: 0,
      },
      1280:{
        // slidesPerView: 1,
        spaceBetween: 0,
      }
    }
  };
  constructor() { 
    SwiperCore.use([Pagination, Autoplay]);
  }

  ngOnInit(): void {
  }

}
