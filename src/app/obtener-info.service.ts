import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { DNI } from './dni'
@Injectable({
  providedIn: 'root'
})
export class ObtenerInfoService {

  url = "https://apiperu.dev/api/dni/"

  constructor(private http: HttpClient) { }


  getUsuario(usuario:string):Observable<DNI> {
      const url = this.url + usuario;
      return this.http.get<DNI>(url);
  }
}
