import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ModalComponent } from './modal/modal.component';
import { SwiperModule } from 'swiper/angular';
import { DitrictsComponent } from './ditricts/ditricts.component';
import { PromoComponent } from './promo/promo.component';
import { HttpClientModule } from '@angular/common/http';
import { PersonaComponent } from './persona/persona.component';

@NgModule({
  declarations: [
    AppComponent,
    ModalComponent,
    DitrictsComponent,
    PromoComponent,
    PersonaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SwiperModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
