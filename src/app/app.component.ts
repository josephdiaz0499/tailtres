import { Component, ViewChild } from '@angular/core';
import SwiperCore, { Pagination, SwiperOptions, Autoplay } from 'swiper/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'tailtres';
  status: boolean = false;

  @ViewChild('newSwiper') newSwiper: any;
  slides = Array.from({ length: 5 }).map(
    (el, index) => `Slide ${index + 1}`
  );


config: SwiperOptions = {
    pagination: {
                  clickable: true,
                
                },       
    mousewheel: true,
    slidesPerView: 'auto',
    spaceBetween: 30,
/*     autoplay: {
      delay: 4000,
    }, */
    breakpoints: {
      0:{
        slidesPerView: 1,
        spaceBetween: 10,
      },
      320: {
        slidesPerView: 1,
        spaceBetween: 10,
      },
      425:{
        slidesPerView: 'auto',
        spaceBetween: 20,
      },
      768: {
        slidesPerView: 2,
        spaceBetween: 40,
      },
      1280: {
        slidesPerView: 3,
        spaceBetween: 25,
      },
      1440:{
        slidesPerView: 4,
        spaceBetween: 20,
      },
    },
  };



  showModal = false;


  constructor() {
    SwiperCore.use([Pagination, Autoplay]);

  }
  toggleModal = () => {
    this.showModal = !this.showModal;
  }

  clickEvent(){
    this.status = !this.status;       
}


}
