const defaultTheme = require('tailwindcss/defaultTheme')
module.exports = {
  purge: {
    // enabled: true,
    // content: [
    //   './src/**/*.{html,ts}',
    // ]
  },
  darkMode: false, // or 'media' or 'class'
  theme: {
    screen:{
      'xs': '475px',
      ...defaultTheme.screens,
    },
    extend: {
      screens: {
        '2xl': '1440px',
      },
      
      colors:{
        gray: {
          50:  '#feffff', 
          100: '#fefeff', 
          200: '#fcfdfe', 
          300: '#fbfbfd', 
          400: '#f7f9fc', 
          500: '#f4f6fb', 
          600: '#dcdde2', 
          700: '#b7b9bc', 
          800: '#929497', 
          900: '#78797b'      
        },
        blue: {
          50:  '#f2f7ff', 
          100: '#e6efff', 
          200: '#bfd6ff', 
          300: '#99beff', 
          400: '#4d8dff', 
          500: '#005cff', 
          600: '#0053e6', 
          700: '#0045bf', 
          800: '#003799', 
          900: '#002d7d'
        },
      },

      borderRadius: {
        '3xl': '1.25rem',
        '4xl': '1.5rem',
      },

      fontFamily: {
        sans: ['Barlow'],
      },

      fontSize:{
        '4xl': ['2rem', { lineHeight: '2.5rem' }],
        '5xl': ['2.25rem', { lineHeight: '2.5rem' }],
        '6xl': ['3rem', { lineHeight: '1' }],
        '7xl': ['3.75rem', { lineHeight: '1' }],
        '8xl': ['4.5rem', { lineHeight: '1' }],
        '9xl': ['6rem', { lineHeight: '1' }],
        '10xl': ['8rem', { lineHeight: '1' }],
      },

      width:{
        '2i': '48%',
        '3i': '32%',
        '4i': '24%',
      },
      maxWidth: {
        '25':'25px',
        '50':'50px',
        '150':'150px',
      },
      padding: {
        '5p': '5%',
        '10p':'10%',
        '15p':'15%',
      },
      margin: {
        '-5p': '-5%',
      },
    },

  },
  variants: {
    extend: {
      fontSize: ['hover', 'focus'],
      justifyContent: ['last'],
      
    },
  },
  experimental: {
    additionalBreakpoint: true,},
  plugins: [require('@tailwindcss/forms')],
}
